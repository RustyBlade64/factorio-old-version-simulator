require("runtime-changes")

local version_strings = require("version_strings")
local version_setting = settings.startup["old-version-simulator_version"].value

function generate_list_of_tech_fixes()
    global.technology_unlocks_to_fix = {}

    if version_setting < version_strings["0.16.27"] then
        fix_refined_concrete(global.technology_unlocks_to_fix)
    end

    if version_setting < version_strings["0.16.0"] then
        fix_buffer_chests(global.technology_unlocks_to_fix)
        fix_cliff_explosives(global.technology_unlocks_to_fix)
        fix_artillery(global.technology_unlocks_to_fix)
    end
end

script.on_init(generate_list_of_tech_fixes)
script.on_configuration_changed(generate_list_of_tech_fixes)

script.on_event(defines.events.on_research_finished, function(args)
    local tech_name = args.research.name

    if not global.technology_unlocks_to_fix[tech_name] then
        return
    end

    for _, recipe in pairs(global.technology_unlocks_to_fix[tech_name]) do
        args.research.force.recipes[recipe].enabled = true;
    end
end)
