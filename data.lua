local v016 = require("prototypes.changes.v016")
local v015 = require("prototypes.changes.v015")

local version_strings = require("version_strings")
local version_setting = settings.startup["old-version-simulator_version"].value

if version_setting < version_strings["0.16.27"] then
    v016.remove_refined_concete()
end

if version_setting < version_strings["0.16.17"] then
    v016.set_roboport_stacksize_to_five()
end

if version_setting < version_strings["0.16.16"] then
    v016.reduce_offshore_pump_hitbox()
end

if version_setting < version_strings["0.16.8"] then
    v016.undo_fluid_wagon_balancing()
    -- barrel balancing undone in data-updates
end

if version_setting < version_strings["0.16.0"] then
    v016.remove_buffer_chest()
    v016.remove_cliff_explosives()
    v016.remove_artillery()

    v016.add_asm1_to_prod_science()
    v016.undo_016_stack_size_changes()
    v016.undo_016_craft_time_changes()
    v016.undo_016_tech_tree_changes()
    v016.set_explosives_result_count_to_1()
end

if version_setting < version_strings["0.15.28"] then
    v015.increase_automation_2_research_time()
    v015.unnormalize_science_req_inserter_capacity()
end

if version_setting < version_strings["0.15.27"] then
    v015.unnormalize_science_req_other_tech()
    v015.switch_modules_to_high_tech_science()
end

if version_setting < version_strings["0.15.20"] then
    v015.increase_fluid_wagon_air_resistance()
end

if version_setting < version_strings["0.15.19"] then
    v015.switch_science_pack_distribution_automation_fluid_wagon()
end

if version_setting < version_strings["0.15.10"] then
    v015.change_steam_to_hot_water()
    v015.undo_speed_3_module_typo()
end

if version_setting < version_strings["0.15.8"] then
    v015.reduce_underground_gear_cost()
end

if version_setting < version_strings["0.15.7"] then
    v015.undo_0_15_7_science_changes()
    v015.undo_0_15_7_craft_time_reduction()
    v015.undo_0_15_7_mining_time_changes()
    v015.set_underground_belt_lengths_to_5()
    v015.increase_flamethrower_turrent_liquid_consumption()
    v015.undo_0_15_7_copy_paste_multiplier_changes()
end

if version_setting < version_strings["0.15.6"] then
    v015.decrease_roboport_construction_area()
end

if version_setting < version_strings["0.15.5"] then
    v015.rereduce_flamethrower_turret_liquid_consumption()
end

if version_setting < version_strings["0.15.4"] then
    v015.reduce_water_usage_sulfuric_acid()
end

if version_setting < version_strings["0.15.3"] then
    v015.allow_productivity_in_kovarex_enrichment()
end
