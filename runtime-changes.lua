function fix_refined_concrete(tech_effects_to_fix)
    local concrete = "concrete"
    tech_effects_to_fix[concrete] = tech_effects_to_fix[concrete] or {}

    table.insert(tech_effects_to_fix[concrete], "refined-concrete")
    table.insert(tech_effects_to_fix[concrete], "refined-hazard-concrete")
end

function fix_buffer_chests(tech_effects_to_fix)
    local logistic_system = "logistic-system"
    tech_effects_to_fix[logistic_system] = tech_effects_to_fix[logistic_system] or {}

    table.insert(tech_effects_to_fix[logistic_system], "logistic-chest-buffer")
end

function fix_cliff_explosives(tech_effects_to_fix)
    local should_keep = settings.startup["old-version-simulator_keep-cliff-explosives"].value

    if shoul_keep then
        return
    end

    local cliff_explosives = "cliff-explosives"
    tech_effects_to_fix[cliff_explosives] = tech_effects_to_fix[cliff_explosives] or {}

    table.insert(tech_effects_to_fix[cliff_explosives], cliff_explosives)
end

function fix_artillery(tech_effects_to_fix)
    local artillery = "artillery"

    tech_effects_to_fix[artillery] = tech_effects_to_fix[artillery] or {}


    table.insert(tech_effects_to_fix[artillery], "artillery-shell")
    table.insert(tech_effects_to_fix[artillery], "artillery-targeting-remote")
    table.insert(tech_effects_to_fix[artillery], "artillery-turret")
    table.insert(tech_effects_to_fix[artillery], "artillery-wagon")
end
