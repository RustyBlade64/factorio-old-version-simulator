local version_strings = require("version_strings")

data:extend({
    {
        type = "string-setting",
        name = "old-version-simulator_version",
        order = "a[version]",
        setting_type = "startup",
        allowed_values = {
            version_strings["0.16.27"],
            version_strings["0.16.17"],
            version_strings["0.16.16"],
            version_strings["0.16.8"],
            version_strings["0.16.0"],
            version_strings["0.15.28"],
            version_strings["0.15.27"],
            version_strings["0.15.20"],
            version_strings["0.15.19"],
            version_strings["0.15.10"],
            version_strings["0.15.8"],
            version_strings["0.15.7"],
            version_strings["0.15.6"],
            version_strings["0.15.5"],
            version_strings["0.15.4"],
            version_strings["0.15.3"],
            version_strings["0.15.2"],
        },
        default_value = version_strings["latest"]
    },
    {
        type = "bool-setting",
        name = "old-version-simulator_keep-cliff-explosives",
        order = "b[special]",
        setting_type = "startup",
        default_value = false
    }
})
