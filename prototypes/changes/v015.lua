local lib = require("prototypes.lib")

local change = {}

function change.increase_automation_2_research_time()
    data.raw.technology["automation-2"].unit.time = 15
end

function change.unnormalize_science_req_inserter_capacity()

    lib.change_science_count_per_unit
    (
        "inserter-capacity-bonus-4",
        {
            ["science-pack-1"] = 2,
            ["science-pack-2"] = 2
        }
    )

    lib.change_science_count_per_unit
    (
        "inserter-capacity-bonus-5",
        {
            ["science-pack-1"] = 2,
            ["science-pack-2"] = 2,
            ["science-pack-3"] = 2
        }
    )

    lib.change_science_count_per_unit
    (
        "inserter-capacity-bonus-6",
        {
            ["science-pack-1"] = 2,
            ["science-pack-2"] = 2,
            ["science-pack-3"] = 2
        }
    )

    lib.change_science_count_per_unit
    (
        "inserter-capacity-bonus-7",
        {
            ["science-pack-1"] = 4,
            ["science-pack-2"] = 4,
            ["science-pack-3"] = 3
        }
    )
end

function change.unnormalize_science_req_other_tech()
    local two_military_packs_table = { ["military-science-pack"] = 2 }
    local two_science_1_packs_table = { ["science-pack-1"] = 2 }
    local two_science_1_2_packs_table =
    {
        ["science-pack-1"] = 2,
        ["science-pack-2"] = 2
    }
    local two_science_1_2_3_packs_table =
    {
        ["science-pack-1"] = 2,
        ["science-pack-2"] = 2,
        ["science-pack-3"] = 2
    }

    lib.change_science_count_per_unit("military-3", two_military_packs_table)
    lib.change_science_count_per_unit("cluster-grenade", two_military_packs_table)
    lib.change_science_count_per_unit("grenade-damage-1", two_military_packs_table)
    lib.change_science_count_per_unit("grenade-damage-2", two_military_packs_table)
    lib.change_science_count_per_unit("grenade-damage-3", two_military_packs_table)
    lib.change_science_count_per_unit("military-4", { ["science-pack-2"] = 2 })

    lib.change_science_count_per_unit("railway", two_science_1_packs_table)
    lib.change_science_count_per_unit("automated-rail-transportation", two_science_1_packs_table)
    lib.change_science_count_per_unit("braking-force-1", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("braking-force-2", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("braking-force-3", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("braking-force-4", two_science_1_2_3_packs_table)
    lib.change_science_count_per_unit("braking-force-5", two_science_1_2_3_packs_table)
    lib.change_science_count_per_unit("braking-force-6", two_science_1_2_3_packs_table)
    lib.change_science_count_per_unit("braking-force-7",
    {
        ["science-pack-1"] = 2,
        ["science-pack-2"] = 2,
        ["science-pack-3"] = 2,
        ["production-science-pack"] = 2
    })

    lib.change_science_count_per_unit("automobilism", two_science_1_packs_table)
    lib.change_science_count_per_unit("advanced-oil-processing", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("combat-robot-damage-3", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("combat-robot-damage-4", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("combat-robot-damage-5", two_science_1_2_packs_table)
    lib.change_science_count_per_unit("combat-robot-damage-6", two_science_1_2_packs_table)
end

function change.switch_modules_to_high_tech_science()
    local function switch_science_pack(tech_name, old_pack, new_pack)
        local ingredients = data.raw.technology[tech_name].unit.ingredients

        for _, ingredient in pairs(ingredients) do
            if ingredient[1] == old_pack then
                ingredient[1] = new_pack
            end
        end
    end

    switch_science_pack("speed-module-3", "high-tech-science-pack", "production-science-pack")
    switch_science_pack("effectivity-module-3", "high-tech-science-pack", "production-science-pack")
end

function change.increase_fluid_wagon_air_resistance()
    data.raw["fluid-wagon"]["fluid-wagon"].air_resistance = 0.05
end

function change.switch_science_pack_distribution_automation_fluid_wagon()
    data.raw.technology["automation-2"].unit.count = 40
    lib.change_science_count_per_unit("automation-2", {["science-pack-1"] = 2})

    data.raw.technology["fluid-wagon"].unit.count = 100
    lib.change_science_count_per_unit("fluid-wagon", {["science-pack-1"] = 2, ["science-pack-2"] = 2})
end

function change.change_steam_to_hot_water()
    -- TODO: fluids cannot be hidden in 0.16, so this does nothing
    --       in 0.17 however this should hide the steam icon from combinators, etc.
    --       check whether this is correct and adjust if necessary
    data.raw.fluid.steam.hidden = true

    data.raw.fluid.water.max_temperature = 1000
    data.raw.fluid.water.gas_temperature = 100

    data.raw.boiler.boiler.output_fluid_box.filter = "water"
    data.raw.boiler["heat-exchanger"].output_fluid_box.filter = "water"

    data.raw.generator["steam-engine"].fluid_box.filter = "water"
    data.raw.generator["steam-turbine"].fluid_box.filter = "water"

    local liquefaction_ingredients = data.raw.recipe["coal-liquefaction"].ingredients

    for _, ingredient in pairs(liquefaction_ingredients) do
        if ingredient.type == "fluid" and ingredient.name == "steam" then
            ingredient.name = "water"
        end
    end
end

function change.undo_speed_3_module_typo()
    lib.change_ingredient_count("speed-module-3", "speed-module-2", 4)
end

function change.reduce_underground_gear_cost()
    lib.change_ingredient_count("fast-underground-belt", "iron-gear-wheel", 20)
    lib.change_ingredient_count("express-underground-belt", "iron-gear-wheel", 40)
end

function change.undo_0_15_7_science_changes()
    local function swap_recipe_ingredient(recipe_name, old_ingredient, new_ingredient)
        local ingredients = data.raw.recipe[recipe_name].ingredients

        for _, ingredient in pairs(ingredients) do
            if ingredient[1] == old_ingredient then
                ingredient[1] = new_ingredient
            end
        end
    end

    -- TODO: make this resistent against asm 1 not existing as ingredient (i.e. in the planned advanced mode)
    swap_recipe_ingredient("production-science-pack", "assembling-machine-1", "pumpjack")
    swap_recipe_ingredient("science-pack-3", "electric-mining-drill", "assembling-machine-1")
end

function change.undo_0_15_7_craft_time_reduction()
    data.raw.recipe.lab.energy_required = 5
    data.raw.recipe.roboport.energy_required = 15
    data.raw.recipe.pumpjack.energy_required =  10
    data.raw.recipe["oil-refinery"].energy_required = 20
    data.raw.recipe["chemical-plant"].energy_required = 10
end

function change.undo_0_15_7_mining_time_changes()
    data.raw["storage-tank"]["storage-tank"].minable.mining_time = 3
    data.raw.reactor["nuclear-reactor"].minable.mining_time = 0.5
end

function change.set_underground_belt_lengths_to_5()
    data.raw["underground-belt"]["fast-underground-belt"].max_distance = 5
    data.raw["underground-belt"]["express-underground-belt"].max_distance = 5
end

function change.increase_flamethrower_turrent_liquid_consumption()
   local flamethrower = data.raw["fluid-turret"]["flamethrower-turret"]

   flamethrower.shoot_in_prepare_state = true
   flamethrower.attack_parameters.fluid_consumption = 2.0
end

function change.undo_0_15_7_copy_paste_multiplier_changes()
    data.raw.recipe.centrifuge.requester_paste_multiplier = nil
    data.raw.recipe["nuclear-reactor"].requester_paste_multiplier = nil
end

function change.decrease_roboport_construction_area()
    data.raw.roboport.roboport.construction_radius = 51
end

function change.rereduce_flamethrower_turret_liquid_consumption()
    data.raw["fluid-turret"]["flamethrower-turret"].attack_parameters.fluid_consumption = 0.2
end

function change.reduce_water_usage_sulfuric_acid()
    lib.change_ingredient_count("sulfuric-acid", "water", 10)
end

function change.allow_productivity_in_kovarex_enrichment()
    table.insert(data.raw.module["productivity-module"].limitation, "kovarex-enrichment-process")
    table.insert(data.raw.module["productivity-module-2"].limitation, "kovarex-enrichment-process")
    table.insert(data.raw.module["productivity-module-3"].limitation, "kovarex-enrichment-process")
end

return change
