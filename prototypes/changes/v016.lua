local lib = require("prototypes.lib")

local change = {}

function change.remove_refined_concete()
    lib.remove_and_hide_tech_unlock("concrete", "refined-concrete")
    lib.remove_and_hide_tech_unlock("concrete", "refined-hazard-concrete")
end

function change.set_roboport_stacksize_to_five()
    data.raw.item.roboport.stack_size = 5
end

function change.reduce_offshore_pump_hitbox()
    local offshore_pump = data.raw["offshore-pump"]["offshore-pump"]
    offshore_pump.tile_height = nil
    -- TODO: only change what was actually changed in game (collison_box[1][2])
    offshore_pump.collision_box = {{-0.6, -0.45}, {0.6, 0.3}}
end

function change.undo_barrel_balancing()
    local function change_fill_barrel_fluid_ingredient_count(recipe, fluid_count)
        for _, ingredient in pairs(recipe.ingredients) do
            if ingredient.type == "fluid" then
                ingredient.amount = fluid_count
            end
        end
    end

    local function change_empty_barrel_fluid_result_count(recipe, fluid_count)
        for _, result in pairs(recipe.results) do
            if result.type == "fluid" then
                result.amount = fluid_count
            end
        end
    end

    local recipes = data.raw.recipe

    local fluid_per_barrel = 250
    local energy_required_per_barrel = 1

    for _, recipe in pairs(recipes) do
        if recipe.subgroup == "fill-barrel" then
            recipe.energy_required = energy_required_per_barrel
            change_fill_barrel_fluid_ingredient_count(recipe, fluid_per_barrel)
        elseif recipe.subgroup == "empty-barrel" then
            recipe.energy_required = energy_required_per_barrel
            change_empty_barrel_fluid_result_count(recipe, fluid_per_barrel)
        end
    end
end

function change.undo_fluid_wagon_balancing()
    local wagon_item = data.raw["fluid-wagon"]["fluid-wagon"]
    local wagon_ingredients = data.raw.recipe["fluid-wagon"].ingredients

    wagon_item.capacity = 75000
    wagon_item.weight = 3000

    for _, ingredient in pairs(wagon_ingredients) do
        if ingredient[1] == "storage-tank" then
            ingredient[2] = 3
        end
    end
end

function change.remove_buffer_chest()
    lib.remove_and_hide_tech_unlock("logistic-system", "logistic-chest-buffer")
end

function change.remove_cliff_explosives()
    local should_keep = settings.startup["old-version-simulator_keep-cliff-explosives"].value

    if should_keep then
        return
    end

    local cliff_explosives = "cliff-explosives"

    -- This will not hide the technology when it is already researched.
    -- https://forums.factorio.com/viewtopic.php?t=57876
    data.raw.technology[cliff_explosives].enabled = false

    lib.remove_and_hide_tech_unlock(cliff_explosives, cliff_explosives)
end

function change.remove_artillery()
    local artillery = "artillery"
    local artillery_range = "artillery-shell-range-1"
    local artillery_speed = "artillery-shell-speed-1"

    data.raw.technology[artillery].enabled = false
    data.raw.technology[artillery_range].enabled = false
    data.raw.technology[artillery_speed].enabled = false

    lib.remove_and_hide_tech_unlock(artillery, "artillery-shell")
    lib.remove_and_hide_tech_unlock(artillery, "artillery-targeting-remote")
    lib.remove_and_hide_tech_unlock(artillery, "artillery-turret")
    lib.remove_and_hide_tech_unlock(artillery, "artillery-wagon")

    -- do not remove the effects
    -- players might spend resources on nothing because the tech cannot be hidden
    -- TODO: fix this in 0.17 by hiding the technology
    -- lib.remove_tech_effects_by_type(artillery_range, "artillery-range")
    -- lib.remove_tech_effects_by_type(artillery_speed, "gun-speed")
end

function change.add_asm1_to_prod_science()
    local prod_science = data.raw.recipe["production-science-pack"]

    table.insert(prod_science.ingredients, {"assembling-machine-1", 1})
end

function change.undo_016_stack_size_changes()
    data.raw.item["nuclear-reactor"].stack_size = 50
end

function change.undo_016_craft_time_changes()
   data.raw.recipe.lab.energy_required = 3
   data.raw.recipe.roboport.energy_required = 10
   data.raw.recipe["oil-refinery"].energy_required = 10
   data.raw.recipe.satellite.energy_required = 3
   data.raw.recipe["nuclear-reactor"].energy_required = 4
end

function change.undo_016_tech_tree_changes()
    local cluster_grenade = "cluster-grenade"

    data:extend{
        {
            type = "technology",
            name = cluster_grenade,
            icon = "__old-version-simulator__/graphics/technology/cluster-grenade.png",
            icon_size = 128,
            effects =
            {
                {
                    type = "unlock-recipe",
                    recipe = "cluster-grenade"
                },
            },
            prerequisites = {"military-4"},
            unit =
            {
                count = 200,
                ingredients =
                {
                    {"science-pack-1", 1},
                    {"science-pack-2", 1},
                    {"military-science-pack", 1},
                    {"science-pack-3", 1},
                    {"high-tech-science-pack", 1},
                },
                time = 45
            },
            order = "e-a-d"
        }
    }

    lib.remove_tech_unlock("military-4", cluster_grenade)

    data.raw.technology["uranium-ammo"].prerequisites = {"military-3"}
end

function change.set_explosives_result_count_to_1()
    data.raw.recipe.explosives.normal.result_count = 1
    data.raw.recipe.explosives.expensive.result_count = 1
end

return change
