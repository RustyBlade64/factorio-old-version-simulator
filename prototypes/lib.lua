local lib = {}

function lib.remove_tech_unlock(technology_name, recipe_name)
    local effects = data.raw.technology[technology_name].effects

    for key, effect in pairs(effects) do
        if effect.type == "unlock-recipe" and effect.recipe == recipe_name then
            effects[key] = nil
        end
    end
end


function lib.remove_and_hide_tech_unlock(technology_name, recipe_name, item_name)
    item_name = item_name or recipe_name

    lib.remove_tech_unlock(technology_name, recipe_name)

    -- TODO: find a better way to do this than blindly checking all possibilities
    local item = data.raw.item[item_name] or data.raw.capsule[item_name] or data.raw.ammo[item_name] or
                 data.raw["item-with-entity-data"][item_name]
    item.flags = item.flags or {}
    table.insert(item.flags, "hidden")

    data.raw.recipe[recipe_name].hidden = true
end

function lib.change_science_count_per_unit(technology, packs_to_change)
    local ingredients = data.raw.technology[technology].unit.ingredients

    for _, ingredient in pairs(ingredients) do
        if packs_to_change[ingredient[1]] ~= nil then
            ingredient[2] = packs_to_change[ingredient[1]]
        end
    end
end

function lib.change_ingredient_count(recipe_name, ingredient_name, count)
    local ingredients = data.raw.recipe[recipe_name].ingredients

    for _, ingredient in pairs(ingredients) do
        local name = ingredient[1] or ingredient.name
        if name == ingredient_name then
            if ingredient[2] then
                ingredient[2] = count
            else
                ingredient.amount = count
            end
        end
    end
end

function lib.remove_tech_effects_by_type(technology_name, effect_types)
    if type(effect_types) == "string" then
        effect_types = {effect_types}
    end

    local effects = data.raw.technology[technology_name].effects

    for key, effect in pairs(effects) do
        for _, effect_type in pairs(effect_types) do
            if effect_type == effect.type then
                effects[key] = nil
            end
        end
    end
end

return lib
