INFO_JSON = info.json

MOD_NAME = $(shell sed -nr 's/.*"name"\s*:\s*"([^"]+)".*/\1/p' $(INFO_JSON))
MOD_VERSION = $(shell sed -nr 's/.*"version"\s*:\s*"([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})".*/\1/p' $(INFO_JSON))

GRAPHICS_DIR = graphics
LOCALE_DIR = locale
BUILD_DIR = build
MOD_SUBDIR = $(MOD_NAME)_$(MOD_VERSION)
TARGET_DIR = $(BUILD_DIR)/$(MOD_SUBDIR)

ARCHIVE_NAME = $(MOD_SUBDIR).zip

LUA_FILES = $(shell find . -name '*.lua')
GRAPHIC_FILES = $(shell find $(GRAPHICS_DIR) -name '*.png')
LOCALE_FILES = $(shell find $(LOCALE_DIR) -name '*.cfg')


.PHONY: build
build: clean package

.PHONY: package
package:
	mkdir -p $(BUILD_DIR)
	mkdir -p $(TARGET_DIR)
	cp --parents $(LUA_FILES) $(GRAPHIC_FILES) $(LOCALE_FILES) $(INFO_JSON) $(TARGET_DIR)
	cd $(BUILD_DIR); zip -r $(ARCHIVE_NAME) $(MOD_SUBDIR)

.PHONY: clean
clean:
	if [ -d $(BUILD_DIR) ]; then rm -r $(BUILD_DIR); fi
