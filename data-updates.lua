local v016 = require("prototypes.changes.v016")

local version_strings = require("version_strings")
local version_setting = settings.startup["old-version-simulator_version"].value

if version_setting < version_strings["0.16.8"] then
    v016.undo_barrel_balancing()
end
